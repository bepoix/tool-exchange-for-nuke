import nuke
import nukescripts
from toolXchange import toolxchange_core


#Add tool to MENU toolbar
menubar = nuke.menu("Nuke")
xchangePane = menubar.addMenu("toolXchange")
xchangePane.addCommand("toolXchange", "toolxchange_core.start()")
