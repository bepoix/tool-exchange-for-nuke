# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\jeremy\.nuke\python\toolXchange\ui\designer\toolxchange_ui.ui',
# licensing of 'C:\Users\jeremy\.nuke\python\toolXchange\ui\designer\toolxchange_ui.ui' applies.
#
# Created: Sat Nov 23 17:48:36 2019
#      by: pyside2-uic  running on PySide2 5.13.1
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(632, 517)
        MainWindow.setWindowTitle("")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame.setFrameShadow(QtWidgets.QFrame.Plain)
        self.frame.setObjectName("frame")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.frame)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.search_lbl = QtWidgets.QLabel(self.frame)
        self.search_lbl.setObjectName("search_lbl")
        self.horizontalLayout.addWidget(self.search_lbl)
        self.search_le = QtWidgets.QLineEdit(self.frame)
        self.search_le.setObjectName("search_le")
        self.horizontalLayout.addWidget(self.search_le)
        self.verticalLayout.addWidget(self.frame)
        self.tv_allClipboard = QtWidgets.QTableWidget(self.centralwidget)
        self.tv_allClipboard.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.tv_allClipboard.setObjectName("tv_allClipboard")
        self.tv_allClipboard.setColumnCount(0)
        self.tv_allClipboard.setRowCount(0)
        self.verticalLayout.addWidget(self.tv_allClipboard)
        self.le_clipboardName = QtWidgets.QLineEdit(self.centralwidget)
        self.le_clipboardName.setObjectName("le_clipboardName")
        self.verticalLayout.addWidget(self.le_clipboardName)
        self.le_clipboardDesc = QtWidgets.QLineEdit(self.centralwidget)
        self.le_clipboardDesc.setObjectName("le_clipboardDesc")
        self.verticalLayout.addWidget(self.le_clipboardDesc)
        self.btn_publish = QtWidgets.QPushButton(self.centralwidget)
        self.btn_publish.setMinimumSize(QtCore.QSize(0, 40))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setWeight(75)
        font.setItalic(False)
        font.setBold(True)
        self.btn_publish.setFont(font)
        self.btn_publish.setAutoFillBackground(False)
        self.btn_publish.setIconSize(QtCore.QSize(16, 16))
        self.btn_publish.setDefault(False)
        self.btn_publish.setFlat(False)
        self.btn_publish.setObjectName("btn_publish")
        self.verticalLayout.addWidget(self.btn_publish)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        self.search_lbl.setText(QtWidgets.QApplication.translate("MainWindow", "search :", None, -1))
        self.le_clipboardName.setPlaceholderText(QtWidgets.QApplication.translate("MainWindow", "name your tool...", None, -1))
        self.le_clipboardDesc.setPlaceholderText(QtWidgets.QApplication.translate("MainWindow", "add a description ...", None, -1))
        self.btn_publish.setText(QtWidgets.QApplication.translate("MainWindow", "PUBLISH", None, -1))

