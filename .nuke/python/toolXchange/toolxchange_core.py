# -*- coding: utf-8 -*-

""" toolXchange is an utility for share tool, script, group, wathever...on Nuke

This tool run nuke-11, nuke-12.  
Pyside 2, python 3.6, windows x64 - 2019-11-23

"""

import sys
import os
import getpass
import uuid
import datetime
import json

from PySide2 import QtWidgets
from PySide2 import QtGui
from PySide2 import QtCore

import nuke
import nukescripts
from collections import OrderedDict
import toolxchange_ui

__author__ = "jeremy bepoix"
__version__ = "0.1.1"
__email__ = "jeremy.bepoix@gmail.com"
REPO_PATH = "G:/02_PRODUCTIONS/repo" 
CURRENT_USER = getpass.getuser()
rootPythonScript = os.path.dirname(__file__)

class txcWin(toolxchange_ui.Ui_MainWindow, QtWidgets.QMainWindow):
	def __init__(self,parent=None):
		super(txcWin, self).__init__(parent)
		# init
		self.setupUi(self)
		# override some visual params
		self.setWindowFlags(self.windowFlags() ^ QtCore.Qt.WindowStaysOnTopHint)
		self.setWindowTitle('toolxchange - ' + __version__)
		icon = QtGui.QIcon()
		icon.addPixmap(QtGui.QPixmap(rootPythonScript + "/ui/icon/mainwindow.png"))
		self.setWindowIcon(icon)

		self.populate()
		# connect signals
		self.btn_publish.clicked.connect(self.publish)
		self.tv_allClipboard.doubleClicked.connect(self.pasteTool)
		self.search_le.textChanged.connect(self.search)
		# creat context menu to get JSON UUID, possibility to remove or open repository
		self.tv_allClipboard.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.tv_allClipboard.customContextMenuRequested.connect(self.context_menu)

	def publish(self):
		"""PUBLISH BUTTON
			- warniing if user forget to put a name or node
			- execute createJson() & populate()
			- clear line Edit name & description
		"""
		cb = QtWidgets.QApplication.clipboard()

		if self.le_clipboardName.text() == "" :
			self.le_clipboardName.setPlaceholderText('please, name your selection ...')
			self.le_clipboardName.setStyleSheet('background-color : rgb(255, 80, 80)')
			QtWidgets.QMessageBox.information(self, 'information', 'name your selection')
			self.le_clipboardName.setStyleSheet('')
			self.le_clipboardName.clear()
			self.le_clipboardDesc.clear()
			return
		if cb.text(mode=cb.Clipboard) == "":
			self.le_clipboardName.setPlaceholderText('please, select object ... \n (CTRL + C)')
			self.le_clipboardName.setStyleSheet('background-color : rgb(255, 80, 80)')
			QtWidgets.QMessageBox.information(self, 'information', 'No nodes selected')
			self.le_clipboardName.setStyleSheet('')
			self.le_clipboardName.clear()
			self.le_clipboardDesc.clear()
			return
		else:
			self.createJson()
			self.populate()
			self.le_clipboardName.setStyleSheet('')
			self.le_clipboardName.clear()
			self.le_clipboardDesc.clear()
			# clear clipboard			
			cb.clear(mode=cb.Clipboard)

	def createJson(self):
		"""CREATE JSON FILE :
			- get some data {user, tool name, comment, date, nukePaste, path, basename}
			- creat a dict()
			- and write json on REPO_PATH
		"""
		now = datetime.datetime.now()
		jsonFile = "%s.json" % uuid.uuid1()
		cb = QtWidgets.QApplication.clipboard()
		script = cb.text()
		doc = dict()
		doc['sender'] = CURRENT_USER
		doc['usersToolName'] = self.le_clipboardName.text()
		doc['usersToolComment'] = self.le_clipboardDesc.text()
		doc['submitted_at'] = str(now)
		doc['toolRawPaste'] = ''' %s ''' % script
		doc['toolPathBaseName'] = REPO_PATH
		doc['tooluuidName'] = jsonFile

		with open(os.path.join(REPO_PATH, jsonFile), 'w') as writeJSON:  
			json.dump(doc, writeJSON)


	def populate(self) :
		"""POPULATE :
			#CONNECT_SIGNALS : context_menu(), get_time_difference_as_string(date)
			- clear Table
			- List all JSON file on REPO_PATH
			- load JSON data as Dict and parse Table
		"""
		self.tv_allClipboard.clear()
		json_files = [pos_json for pos_json in os.listdir(REPO_PATH) if pos_json.endswith('.json')]
		json_files.sort(reverse=True)
		for index, js in enumerate(json_files):
			with open(os.path.join(REPO_PATH, js)) as json_file:
				self.jsondata = json.load(json_file)
				# creat table UI
				headerCol = ['Author', 'Tool', 'Description', 'Date', 'toolpath', 'toolName', 'toolRaw']
				rows = len(json_files)
				cols = len(headerCol)
				self.tv_allClipboard.setRowCount(rows)
				self.tv_allClipboard.setColumnCount(cols)
				self.tv_allClipboard.setHorizontalHeaderLabels(headerCol)
				self.tv_allClipboard.horizontalHeader().setDefaultSectionSize(100)
				self.tv_allClipboard.horizontalHeader().setStretchLastSection(True)
				self.tv_allClipboard.setColumnWidth(0, 100)
				self.tv_allClipboard.setColumnWidth(1, 180)
				self.tv_allClipboard.setColumnWidth(2, 250)
				self.tv_allClipboard.setColumnWidth(3, 80)
				# put JSON data on table
				for col, data in enumerate(self.jsondata):
					sender_itm = QtWidgets.QTableWidgetItem(self.jsondata['sender'])
					userToolName_itm = QtWidgets.QTableWidgetItem(self.jsondata['usersToolName'])
					usersToolComment_itm = QtWidgets.QTableWidgetItem(self.jsondata['usersToolComment'])
					submitted_at_itm = QtWidgets.QTableWidgetItem(self.get_time_difference_as_string(self.jsondata['submitted_at']))
					toolPathBaseName_itm = QtWidgets.QTableWidgetItem(self.jsondata['toolPathBaseName'])
					tooluuidName_itm = QtWidgets.QTableWidgetItem(self.jsondata['tooluuidName'])
					toolRawPaste_itm = QtWidgets.QTableWidgetItem(self.jsondata['toolRawPaste'])
					self.tv_allClipboard.setItem(index, 0, sender_itm )
					self.tv_allClipboard.setItem(index, 1, userToolName_itm )
					self.tv_allClipboard.setItem(index, 2, usersToolComment_itm )
					self.tv_allClipboard.setItem(index, 3, submitted_at_itm )
					self.tv_allClipboard.setItem(index, 4, toolPathBaseName_itm )
					self.tv_allClipboard.setItem(index, 5, tooluuidName_itm )
					self.tv_allClipboard.setItem(index, 6, toolRawPaste_itm )
				# hide column useless for user end, but usefull to request on other method
				self.tv_allClipboard.setColumnHidden(4, True)
				self.tv_allClipboard.setColumnHidden(5, True)
				self.tv_allClipboard.setColumnHidden(6, True)
				self.tv_allClipboard.verticalHeader().hide()			

	def get_time_difference_as_string(self, date):
		"""DATE :
			*note : dependent populate() > submitted_at column index 3

			- JSON can't save date, so convert it with datetime
			- delta time : days, hours, min, sec ago posting
		"""
		delta = datetime.datetime.today() - datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S.%f")
		if delta.days:
			return "%s day(s)" % delta.days
		seconds = delta.seconds
		if seconds < 60:
			return "A few seconds ago"
		elif seconds < 3600:
			return "%s minute(s) ago" % (seconds/60)
		elif seconds < 86400:
			return "%s hours(s) ago" % (seconds/3600)

	def context_menu(self):
		"""CONNECT_SIGNALS : remove()
			*note : dependent populate() > context_menu
			- creat context menu with the JSON UUID if user want to do something with it
		"""
		menu = QtWidgets.QMenu(self)
		tipsRow = self.tv_allClipboard.currentRow()
		menu.addAction(QtGui.QIcon(rootPythonScript + "/ui/icon/view.png"),self.tv_allClipboard.item(tipsRow,5).text())
		menu.addAction(QtGui.QIcon(rootPythonScript + "/ui/icon/remove.png"), 'remove', self.remove)
		menu.addAction(QtGui.QIcon(rootPythonScript + "/ui/icon/open.png"), 'open repository', self.openRepo)
		menu.exec_(QtGui.QCursor.pos())

	def remove(self):
		"""REMOVE CONTEXT MENU :
			- find user attempt to delete IS the Author
			- with hidden column data : "toolPathBaseName" & "tooluuidName" delete JSON file
			- update Table > populate()
		"""
		currRow = self.tv_allClipboard.currentRow()
		usrSel = os.path.join(self.tv_allClipboard.item(currRow,4).text(), self.tv_allClipboard.item(currRow,5).text())
		if CURRENT_USER == self.tv_allClipboard.item(currRow,0).text():
			os.remove(usrSel)
			self.populate()
		else:
			QtWidgets.QMessageBox.information(self, 'information', 'you are not the Author of this tool' )
			return

	def openRepo(self):
		"""OPEN REPOSITORY CONTEXT MENU : :
			- open repo explorer
		"""
		os.startfile(REPO_PATH)

	def search(self):
		"""MATCH FILTER :
			- highlight row if user search match & update top windows title
		"""
		if self.search_le.text() != "":
			self.setWindowTitle('toolxchange | Searching for :' + self.search_le.text())
			items = self.tv_allClipboard.findItems(self.search_le.text(), QtCore.Qt.MatchContains)
			if items:
				for item in items :
					self.tv_allClipboard.selectRow(item.row())
			else:
				print('Found Nothing')
		else:
			self.setWindowTitle('toolxchange - ' + __version__)

	def pasteTool(self):
		"""PASTETOOL :
			#CONNECT_SIGNALS : see top > doubleClicked.connect
			- double click on selected row and paste nuke script
		"""
		rowTool = self.tv_allClipboard.currentRow()
		nukescripts.clear_selection_recursive()
		cb = QtWidgets.QApplication.clipboard()
		cb.setText(self.tv_allClipboard.item(rowTool, 6).text())
		nuke.nodePaste('%clipboard%')
		cb.clear(mode=cb.Clipboard)

def start():
    start.txcWin = txcWin()
    start.txcWin.show()

